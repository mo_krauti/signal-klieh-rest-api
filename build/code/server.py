from flask import Flask, jsonify, request, abort, make_response
from flask_sqlalchemy import SQLAlchemy
from os.path import isfile
import signal1

NUM = 'YOUR_PHONE_NUMBER'

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////root/.local/share/signal-klieh-dbus/msgs.db'
db = SQLAlchemy(app)


class Message(db.Model):
    __tablename__ = 'messages'
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.Integer) #unix epoch seconds utc
    source = db.Column(db.String(20))
    text = db.Column(db.String(500))
    target = db.Column(db.String(30))
    def __repr__(self):
        return str([self.id, self.timestamp, self.source, self.target, self.text])
    def dict(self):
         return {'id':self.id, 'timestamp':self.timestamp, 'source':self.source, 'target':self.target, 'text':self.text}

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'status':'Not found'}), 404)
@app.errorhandler(500)
def not_found(error):
    return make_response(jsonify({'status':'Internal server error'}), 500)

@app.route('/msg')
def allMsgs():
    return jsonify({'status':'Success', 'msgs': [i.dict() for i in db.session.query(Message).all()]})

@app.route('/msg/new/<timestamp>')
def newMsgs(timestamp):
    return jsonify({'status':'Success', 'msgs': [i.dict() for i in db.session.query(Message).filter((Message.timestamp > timestamp)).all()]})

@app.route('/msg/<path:chat>')
def chatMsgs(chat):
    return jsonify({'status':'Success', 'msgs': [i.dict() for i in db.session.query(Message).filter((((Message.source == NUM) & (Message.target == chat)) | (Message.target == chat))).all()]})

@app.route('/send', methods = ['POST'])
def send():
    if not request.json:
        abort(400)
    obj = request.get_json()
    signal1.sendMsg(obj['target'], obj['text'])
    return jsonify({'status':'Success'})

if __name__ == '__main__':
    if isfile('/root/.local/share/signal-klieh-dbus/msgs.db') == False:
        db.create_all()
    app.run(host='0.0.0.0')
